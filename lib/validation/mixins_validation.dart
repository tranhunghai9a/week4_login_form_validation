mixin CommonValidation {
  String? validateEmail(String? value) {
    if (!value!.contains('.')) {
      return 'It must contain dot';
    }
    if (!value!.contains('@')) {
      return 'It must contain @';
    }
    return null;
  }

  String? validatePassword(String? value) {

    if (!value!.contains(RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[!@#$%^&*~]).{8,}$'))) {
      return 'Password must be at least 8 characters includes 1 Uppercase, 1 lowercase and 1 special character.';
    }


    return null;
  }
}