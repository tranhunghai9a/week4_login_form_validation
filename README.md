# week4_form

Viết Flutter App có màn hình Login (gồm có email address và password). Trong đó thực hiện validation như sau:
- Email address: nếu không có dấu @ và dấu . thì báo lỗi (bạn tự thiết kế nội dung báo lỗi)
- Password nếu không tuân thủ qui tắc sau thì báo lỗi:
  + Có ít nhất 1 kí tự Hoa.
  + Có ít nhất 1 kí tự thường.
  + Có ít nhất 1 kí tự đặc biệt.
  + Độ dài ít nhất là 8 kí tự

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
